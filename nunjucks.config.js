const moment = require('moment-timezone')
const fetch = require('node-fetch')
const html2plaintext = require('html2plaintext')

module.exports = {
  root: './src',
  data: getData
}

async function getData (_pathInfo) {
  let api = new ApiClient('https://directus-renee-u21722.vm.elestio.app', 'Europe/Zurich', 6)
  let homepage = await api.getHomepage()

  return {
    homepage,
  }
}

class ApiClient {
  url
  timeZone
  dayOffset

  constructor (url, timeZone, dayOffset) {
    this.url = url
    this.timeZone = timeZone
    this.dayOffset = dayOffset
  }

  async getHomepage () {
    let response = await this._get('/items/website_reneecultureclub?single=1')
    return {
      html_title: response.data['html_title'],
      html_description: response.data['html_description'],
      html_keywords: response.data['html_keywords'],
      text_intro: response.data['text_intro'],
      text_main: response.data['text_main'],
      text_thankyou: response.data['text_thankyou'],
      imprint: response.data['imprint'],
      google_tag_id: 'G-6E9Q6V7LBD',
    }
  }

  async _get (path) {
    let url = this.url + path
    let response = await fetch(url, {method: 'get'})
    let response_json = await response.json();
    let response_errors = response_json['errors']
    if (response_errors) {
      throw new Error(`API error: ${JSON.stringify(response_errors)}`)
    }
    return response_json
  }
}
